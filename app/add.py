#!/usr/bin/env python3
import time
import logging

formatter = '%(message)s'
logging.basicConfig(level=logging.DEBUG, format=formatter)

sum = 0
addnum = 3

while True:
    sum += addnum
    logging.debug(sum)
    time.sleep(3)
